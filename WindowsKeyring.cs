﻿using AdysTech.CredentialManager;

using System.Net;

namespace XLKeePassXC
{
    internal class WindowsKeyring : IKeyring
    {
        public NetworkCredential Load(string target)
        {
            return CredentialManager.GetCredentials(target);
        }

        public void Store(string target, NetworkCredential credential)
        {
            _ = CredentialManager.SaveCredentials(target, credential);
        }
    }
}
