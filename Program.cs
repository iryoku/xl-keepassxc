﻿using System.CommandLine;
using System.Diagnostics;
using System.Net.Sockets;
using System.Runtime.Versioning;
using System.Text;
using System.Text.RegularExpressions;

namespace XLKeePassXC;

internal class Program
{
    [SupportedOSPlatformGuard("windows")]
    private static readonly bool isWindows = OperatingSystem.IsWindows();

    private static int Main(string[] args)
    {
        var rootCommand = new RootCommand("XIV Launcher KeePassXC Bridge");

        var uuidArgument = new Argument<string>(
            "uuid",
            "UUID of the database entry to access.");
        rootCommand.Add(uuidArgument);

        var launcherOption = new Option<string?>(
            "--path",
            "Path to XIV Launcher.");
        rootCommand.Add(launcherOption);

        var timeoutOption = new Option<double>(
            "--timeout",
            () => 60,
            "Maximum time to wait, in seconds.");
        rootCommand.Add(timeoutOption);
        rootCommand.SetHandler(
            (Action<string, string, double>)HandleLaunch,
            uuidArgument, launcherOption, timeoutOption);

        if (isWindows)
        {
            var shortcutCommand = new Command(
                "create-shortcut",
                "Create a shortcut on your desktop that silently runs\nthis utility.");
            rootCommand.Add(shortcutCommand);
            shortcutCommand.SetHandler(
                (Action<string, string?, double>)HandleCreateShortcut,
                uuidArgument, launcherOption, timeoutOption);
        }

        return rootCommand.Invoke(args);
    }

    private static void HandleLaunch(
        string uuid, string? launcher, double timeoutSeconds)
    {
        launcher = Environment.ExpandEnvironmentVariables(
            string.IsNullOrWhiteSpace(launcher) ?
                GetDefaultLauncerPath() :
                launcher);
        launcher = GetVersionedPath(launcher);

        var process = Process.Start(launcher);
        var timeout = TimeSpan.FromSeconds(timeoutSeconds);

        SendTotp(process, uuid, timeout);
    }

    [SupportedOSPlatform("windows")]
    private static void HandleCreateShortcut(
        string uuid, string? launcher, double timeoutSeconds)
    {
        var desktop = Environment.GetFolderPath(
            Environment.SpecialFolder.Desktop);
        var shortcut = Path.Combine(desktop, "XIV Launcher.lnk");
        var executable = Environment.ProcessPath;
        var workingDirectory = Path.GetDirectoryName(executable);
        executable = Path.GetFileName(executable);
        var startCommand = "Start-Process -WindowStyle Hidden -FilePath " +
            $"{EncodeArgument(executable)} -ArgumentList {uuid}";
        if (timeoutSeconds != 60)
        {
            startCommand += $", --timeout, {timeoutSeconds}";
        }
        if (!string.IsNullOrWhiteSpace(launcher))
        {
            if (!string.IsNullOrWhiteSpace(workingDirectory))
            {
                launcher = Path.GetRelativePath(workingDirectory, launcher);
            }
            startCommand += $", --path, {EncodeArgument(launcher)}";
        }
        var icon = Environment.ExpandEnvironmentVariables(
            string.IsNullOrWhiteSpace(launcher) ?
                GetDefaultLauncerPath() :
                launcher);
        Shell.CreateLink(
            shortcut, "powershell.exe",
            $"-Command {EncodeArgument(startCommand)}",
            icon, workingDirectory);
    }

    private static string GetDefaultLauncerPath() =>
        @"%LocalAppData%\XIVLauncher\XIVLauncher.exe";

    private static string GetVersionedPath(string path) => Path.Combine(
        Path.GetDirectoryName(path) ?? string.Empty,
        "app-" + FileVersionInfo.GetVersionInfo(path).ProductVersion,
        Path.GetFileName(path));

    private static void SendTotp(
        Process process, string uuid, TimeSpan timeout)
    {
        var timer = Stopwatch.StartNew();

        while (!process.HasExited)
        {
            try
            {
                using var socket = new Socket(
                    SocketType.Stream, ProtocolType.Tcp);
                var result = socket.BeginConnect(
                    "localhost", 4646, null, null);
                using var wait = result.AsyncWaitHandle;
                if (!wait.WaitOne(timeout - timer.Elapsed, false))
                {
                    socket.Close();
                    throw new TimeoutException();
                }
                socket.EndConnect(result);

                var kpxc = new KeePassXCServer();
                kpxc.Associate(new WindowsKeyring());
                var totp = kpxc.GetTotp(uuid);
                var request =
                    $"GET /ffxivlauncher/{totp} HTTP/1.0\r\n" +
                    "Host: localhost\r\n" +
                    "User-Agent: XL-KeePassXC\r\n" +
                    "Content-Length: 0\r\n" +
                    "\r\n";
                _ = socket.Send(Encoding.UTF8.GetBytes(request));

                return;
            }
            catch (SocketException ex) when (ex.SocketErrorCode switch
            {
                SocketError.ConnectionRefused => true,
                _ => false,
            })
            { }
        }
    }

    public static string EncodeArgument(string? arg)
    {
        if (string.IsNullOrEmpty(arg))
            return string.Empty;
        string value = Regex.Replace(arg, @"(\\*)" + "\"", @"$1\$0");
        value = Regex.Replace(value, @"^(.*\s.*?)(\\*)$", "\"$1$2$2\"");
        return value;
    }
}
