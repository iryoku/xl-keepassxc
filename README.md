# XIV Launcher KeePassXC Bridge
A small utility that automatically starts XIV Launcher and sends a
time-based one-time password (TOTP) retrieved from KeePassXC via the
KeePassXC-Browser protocol.

## Usage
Browser Integration must be enabled for at least one browser in KeePassXC
for this utility to work. You will also need to get the UUID for the
database entry you wish to use from KeePassXC. Then, simply pass the UUID
on the command line.
```
> xl-keepassxc cbd44efb011d48bcad4a13f0f56907ae
```
The first time you run this command, KeePassXC will prompt you to give the
utility a name and grant it permission to access your password database.
You will also automatically be prompted to unlock the database as
necessary.

Use `--help` for a full list of available command line options.
```
> xl-keepassxc --help
Description:
  XIV Launcher KeePassXC Bridge

Usage:
  xl-keepassxc <uuid> [command] [options]

Arguments:
  <uuid>  UUID of the database entry to access.

Options:
  --path <path>        Path to XIV Launcher.
  --timeout <timeout>  Maximum time to wait, in seconds. [default: 60]
  --version            Show version information
  -?, -h, --help       Show help and usage information


Commands:
  create-shortcut  Create a shortcut on your desktop that silently runs
                   this utility.
```
