﻿using NaCl;

using System.Globalization;
using System.IO.Pipes;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json.Nodes;

namespace XLKeePassXC;

internal class KeePassXCServer
{
    private readonly string clientId;
    private readonly NamedPipeClientStream pipe;
    private readonly Curve25519XSalsa20Poly1305 cipher;
    private readonly string publicKey;

    public KeePassXCServer()
    {
        Span<byte> buffer = stackalloc byte[24];
        RandomNumberGenerator.Fill(buffer);
        clientId = Convert.ToBase64String(buffer);

        pipe = new NamedPipeClientStream(string.Format(
            CultureInfo.InvariantCulture,
            "org.keepassxc.KeePassXC.BrowserServer_{0}",
            Environment.GetEnvironmentVariable("USERNAME")));

        pipe.Connect(1);

        Span<byte> clientPrivateKey = stackalloc byte[32];
        Span<byte> clientPublicKey = stackalloc byte[32];
        Curve25519XSalsa20Poly1305.KeyPair(
            clientPrivateKey, clientPublicKey);

        publicKey = Convert.ToBase64String(clientPublicKey);

        Span<byte> nonce = stackalloc byte[24];
        RandomNumberGenerator.Fill(nonce);
        var message = new JsonObject
        {
            ["action"] = "change-public-keys",
            ["publicKey"] = Convert.ToBase64String(clientPublicKey),
            ["nonce"] = Convert.ToBase64String(nonce),
            ["clientID"] = clientId,
        };
        pipe.Write(Encoding.UTF8.GetBytes(message.ToJsonString()));
        buffer = stackalloc byte[0x100000];
        var response = JsonNode.Parse(buffer[..pipe.Read(buffer)])!;
        cipher = new Curve25519XSalsa20Poly1305(
            clientPrivateKey,
            Convert.FromBase64String((string)response["publicKey"]!));
    }

    public void Associate(IKeyring keyring)
    {
        var target = "XLKeePassXC:" + GetHash();
        var credential = keyring.Load(target);
        if (credential == null || !TestAssociation(credential))
        {
            keyring.Store(target, Associate());
        }
    }

    public string GetTotp(string uuid)
    {
        var response = Send(new JsonObject
        {
            ["action"] = "get-totp",
            ["uuid"] = uuid,
        });
        Check(response);
        return (string)response["totp"]!;
    }

    private string GetHash()
    {
        var response = Send(new JsonObject
        {
            ["action"] = "get-databasehash",
        });
        Check(response);
        return (string)response["hash"]!;
    }

    private bool TestAssociation(NetworkCredential credential)
    {
        var response = Send(new JsonObject
        {
            ["action"] = "test-associate",
            ["id"] = credential.UserName,
            ["key"] = credential.Password,
        });
        Check(response);
        return (string?)response["success"] == "true";
    }

    private NetworkCredential Associate()
    {
        Span<byte> buffer = stackalloc byte[32];
        RandomNumberGenerator.Fill(buffer);
        var key = Convert.ToBase64String(buffer);
        var response = Send(new JsonObject
        {
            ["action"] = "associate",
            ["key"] = publicKey,
            ["idKey"] = key,
        });
        Check(response);
        return new()
        {
            UserName = (string)response["id"]!,
            Password = key,
        };
    }

    private JsonNode Send(JsonNode message)
    {
        JsonNode response;
        while (true)
        {
            Transmit(message);
            response = Receive();
            if (response["success"] != null || (string?)response["errorCode"] != "1")
            {
                break;
            }
            while ((string?)Receive()["action"] != "database-unlocked") { }
        }
        return response;
    }

    private void Transmit(JsonNode message)
    {
        Span<byte> nonce = stackalloc byte[24];
        RandomNumberGenerator.Fill(nonce);
        var plaintext = Encoding.UTF8.GetBytes(message.ToJsonString());
        Span<byte> ciphertext = stackalloc byte[
            plaintext.Length + Curve25519XSalsa20Poly1305.TagLength];
        cipher.Encrypt(ciphertext, plaintext, nonce);
        var wrapped = new JsonObject
        {
            ["action"] = message["action"]!.GetValue<string>(),
            ["message"] = Convert.ToBase64String(ciphertext),
            ["nonce"] = Convert.ToBase64String(nonce),
            ["clientID"] = clientId,
            ["triggerUnlock"] = "true",
        };
        pipe.Write(Encoding.UTF8.GetBytes(wrapped.ToJsonString()));
    }

    private JsonNode Receive()
    {
        Span<byte> buffer = stackalloc byte[0x100000];
        var response = JsonNode.Parse(buffer[..pipe.Read(buffer)])!;
        var message = (string?)response["message"];
        if (message == null)
        {
            return response;
        }
        Span<byte> ciphertext = Convert.FromBase64String(message);
        Span<byte> nonce = Convert.FromBase64String((string)response["nonce"]!);
        Span<byte> plaintext = stackalloc byte[
            ciphertext.Length - Curve25519XSalsa20Poly1305.TagLength];
        return !cipher.TryDecrypt(plaintext, ciphertext, nonce)
            ? throw new Exception("Failed to decrypt response")
            : JsonNode.Parse(plaintext)!;
    }

    private static void Check(JsonNode response)
    {
        if (response["errorCode"] != null)
        {
            throw new Exception((string)response["error"]!);
        }
    }
}
