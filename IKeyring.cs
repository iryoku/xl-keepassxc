﻿using System.Net;

namespace XLKeePassXC
{
    internal interface IKeyring
    {
        public NetworkCredential Load(string Target);

        public void Store(string Target, NetworkCredential credential);
    }
}
